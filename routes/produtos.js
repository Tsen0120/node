const express = require('express');
const router = express.Router();

// Retorna todos os produtos
router.get('/', (req, res, next) => {
    res.status(200).send({
        mensagem: 'Retorna todos os Produtos'
    });
});

// Insere um produtos
router.post('/', (req, res, next) => {
    const produto = {
        nome_do_produto: req.body.nome,
        preco_do_produto: req.body.preco,
    };
    res.status(201).send({
        mensagem: 'Insere um produto',
        produtoCriado: produto
    })
});

//Retorna os dados de um produto
router.get('/:id_produto', (req, res, next) => {
    const id = req.params.id_produto

    if (id === 'especial') {
        res.status(200).send({
            mensagem: 'GET de um produto exclusivo',
            id: id
        });
    } else {
        res.status(200).send({
            mensagem: 'Voce passou um ID'
        });
    }
})
// Altera um produto
router.patch('/', (req, res, next) => {
    res.status(201).send({
        mensagem: 'Produto alterado'
    })
});
// Deleta um produto
router.delete('/', (req, res, next) => {
    res.status(201).send({
        mensagem: 'Produto excluido'
    })
});

module.exports = router;
